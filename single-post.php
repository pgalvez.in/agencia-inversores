	<?php

	/*Template Name: ACSJ_Theme */

	?>

    <?php get_header();?>
	<section class="full-heiht">
        <article class="mb-4 inside-header news">
            <h3 class="page-header-title">Noticias</h3>
        </article>
        <article>
            <div class="container">
                <div class="row">
                    <div class="col">
                    <?php $the_query = new WP_Query(
					array(
                        'post_type'			=> 'post',	
                        'posts_per_page'	=> '1',	
                        'p'         => get_the_ID(),
						)); ?>
                    <?php
                    	if ( $the_query->have_posts() ) {
					    while ( $the_query->have_posts() ) {
						$the_query->the_post();
                        ?>
                    
                        <div class="box-new">
                            <div class="news-item-img">
                                 <img src="<?php $nuevo="h=auto&w=auto&zc=1&q=80";  echo get_image('noticia_imagen',1,1,0,NULL,$nuevo); ?>" alt="">
                            </div>
                            <div class="news-item-txt">
                                <h4 class="news-item-title">
                                     <?php echo get_the_title(); ?>
                                </h4>
                                <div class="news-item-fecha">
                                    <i class="fas fa-calendar-alt color-primary mr-2"></i>
                                    <span><?php echo get_the_date(); ?></span>
                                </div>
                                <div class="news-item-contenido">
                                    <?php echo get_the_content(); ?>  
                                </div>
                            </div>
                            <a href="<?php echo get_page_link('14');?>" class="btn btn-line float-right mt-5 mb-5">VER TODAS</a>
                        </div>
                        <?php } } ?>
                    </div>
                </div>
            </div>
        </article>
    </section>
	<?php get_footer();?>