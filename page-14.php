	<?php

	/*Template Name: ACSJ_Theme */

	?>

	<?php get_header();?>
	 <section class="full-heiht">
        <article class="mb-4 inside-header departamento">
            <h3 class="page-header-title">VER TODAS</h3>
        </article>
        <article>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h3 class="noticias-titular">Noticias</h3>
                        <?php $the_query = new WP_Query(
					    array(
						'post_type'			=> 'post',			
						)); ?>

                        <?php 
                        if ( $the_query->have_posts() ) {
                            while ( $the_query->have_posts() ) {
                                $the_query->the_post();
                                ?>
                                <a href="<?php echo get_page_link();?>" class="d-block new-link">
                                    <div class="list-news-item">
                                        <div class="list-news-item-img">
                                            <img src="<?php $nuevo="h=auto&w=auto&zc=1&q=80";  echo get_image('noticia_imagen',1,1,0,NULL,$nuevo); ?>" alt="">
                                        </div>
                                        <div class="list-news-item-txt">
                                            <h4 class="list-news-item-title">
                                                <?php echo get_the_title(); ?>
                                            </h4>
                                              <div class="list-news-item-fecha">
                                                <i class="fas fa-calendar-alt color-primary mr-2"></i>
                                            <span><?php echo get_the_date(); ?></span>
                                            </div>
                                            <div class="list-news-item-contenido">
                                            <?php echo get_the_content(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </a>


                                <?php } } ?>
                    </div>
                    
                </div>
                <!-- <div class="row">
                    <div class="col">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination pagination-lg justify-content-center pagination-style">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">1</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">2</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">3</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">4</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">5</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">FIN</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
               
                </div> -->
            </div>
        </article>
    </section>
	<?php get_footer();?>