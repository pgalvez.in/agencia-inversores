 <footer>
    
        <article class="info-box">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <p>Copyright © 2018 Agencia Calidad San Juan. Todos los derechos reservados. 25 de Mayo 577 Este, San Juan, Argentina.
                            Tel: (0264) 429 6282</p>
                    </div>
                </div>
            </div>
        </article>
        <article class="copy-right">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <p>Desarrollado por
                            <strong>CUYUM</strong>
                        </p>
                    </div>
                </div>
            </div>
        </article>
    
    
    </footer>


    <!-- ========== JS ========= -->

<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/jquery/jquery.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/bootstrap/js/bootstrap.bundle.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/bootstrap/js/bootstrap.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/fontawesome/js/fontawesome-all.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/main-script.js"></script>

<?php wp_footer(); ?>
</body>
</html>