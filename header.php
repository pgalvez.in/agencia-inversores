<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="description" content="Inversores San Juan">
	<meta name="author" content="Pablo Galvez">
	<meta name="keywords" content="ANALIZAR CONTENIDO DE KEYWORDS">
	<meta charset="utf-8">
	<title>Agencia Calidad</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="shortcut icon" href="">

	<!-- CSS -->
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/fontawesome/css/fa-svg-with-js.css">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/main.css">

	<!-- FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
     <link href="https://fonts.googleapis.com/css?family=Poppins:100,300,500" rel="stylesheet">

	
	<link rel="stylesheet" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<?php wp_head(); ?>
</head>
<body>

 <header class="header ">
        <nav class="navbar navbar-expand-lg navbar-light nav-bar-center">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="" id="navbarSupportedContent">
                <!-- <ul class="navbar-nav mr-auto nav-menu menu-style">
                    <li class="nav-item active">
                        <a class="nav-link menu-item" href="#news">Noticias
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link menu-item" href="#select">Departamentos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link menu-item" href="#appland">App</a>
                    </li>
                </ul> -->
                <?php

				$defaults = array(
					'menu'            => 'main',
					'menu_class'      => 'navbar-nav mr-auto nav-menu menu-style',
					'menu_id'         => 'main-menu',
					'echo'            => true,
					'fallback_cb'     => 'wp_page_menu',
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'depth'           => 0,
					'walker'          => ''
				);

				wp_nav_menu( $defaults );

				?>
            </div>
            

            <a href="<?php echo get_site_url();?>" class="logo">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/adel-logo.png" alt="">
            </a>

        </nav>
    </header>




    
	