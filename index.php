﻿<?php

/*Template Name: ACSJ_Theme */

 get_header(); ?>

  <section class="section-item news" id="news">
        <article class="mb-4">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="page-header">
                            <h3 class="page-header-title">Noticias</h3>
                        </div>
                    </div>
                </div>
        </article>
        <article class="home-news">
            <div class="container">
                <div class="row">
                    <div class="col-10">

                <?php query_posts( 'post_type=noticia' );?>
				<?php $the_query = new WP_Query(
					array(
						'post_type'			=> 'post',
						'posts_per_page'	=> '3',				
						)); ?>

                <?php 
              
             
				if ( $the_query->have_posts() ) {
					while ( $the_query->have_posts() ) {
						$the_query->the_post();
                        ?>
                         <a href="<?php echo get_page_link();?>" class="d-block new-link">
                            <div class="list-news-item">
                                <div class="list-news-item-img">
                                    <img src="<?php $nuevo="h=auto&w=auto&zc=1&q=80";  echo get_image('noticia_imagen',1,1,0,NULL,$nuevo); ?>" alt="">
                                </div>
                                <div class="list-news-item-txt">
                                    <h4 class="list-news-item-title">
                                        <?php echo get_the_title(); ?>
                                    </h4>
                                    <div class="list-news-item-fecha">
                                        <i class="fas fa-calendar-alt color-primary mr-2"></i>
                                       <span><?php echo get_the_date(); ?></span>
                                    </div>
                                    <div class="list-news-item-contenido">
                                        <?php echo get_the_content(); ?> 
                                    </div>
                                </div>
                            </div>
                        </a>


						<?php } } ?>

                    </div>
                    <div class="col-2">
                        <a href="<?php echo get_page_link('14');?>" class="btn btn-vertodas">VER TODAS</a>
                    </div>
                </div>
            </div>
        </article>
    </section>
    <section class="section-item select" id="select">
        <article class="mb-4">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="page-header">
                            <h3 class="page-header-title">Departamentos</h3>
                        </div>
                    </div>
                </div>
        </article>
        <article class="select-news absolute-center">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="box-select">
                            <h4 class="select-text separe">Seleccione el departamento al que desea acceder</h4>
                            
                            <form method="GET" action="#">
                                <div class="form-group separe">
                                    <select class="form-control form-control-lg select-box" id="select-depa">
                                    <option value="">Seleccione departamento</option>
                                         <?php query_posts( 'post_type=departamento' );?>
                                            <?php $the_query = new WP_Query(
                                                array(
                                                    'post_type'			=> 'departamento',	
                                                    )); ?>

                                            <?php 
                                            if ( $the_query->have_posts() ) {
                                                while ( $the_query->have_posts() ) {
                                                    $the_query->the_post();
                                                    ?>
                                                        <option value="<?php echo get_page_link();?>"> <?php echo get('nombre_departamento'); ?></option>

                                                    <?php } } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                              
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </section>
    <section class="section-item appland" id="appland">
        <article class="mb-4">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="page-header app">
                            <h3 class="page-header-title">App</h3>
                        </div>
                    </div>
                </div>
        </article>
        <article class="absolute-center app-box">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h4 class="hight-text">¿Necesitas soporte para tu emprendimiento productivo?</h4>
                        <h5 class="bold-text">REGISTRATE EN NUESTRA APP</h5>
                        <a target="_blank" href="http://www.reddeagenciasdesarrollolocal.com.ar/Beneficio" class="btn btn-acceder">Acceder</a>
                    </div>
                </div>
            </div>
        </article>
    </section>

<?php get_footer();?>
